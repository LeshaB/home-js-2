function chengeNumberToString(numb) {
    if (typeof numb === 'number') { // перевірка на тип данних, що аргумент являється числом
        return string(numb); // повернення з функції конверторного аргумента в строку
    }
    return 'argument is not a number'; // повернення строки якщо аргумент не намбер
}

function checkIfParnost(numb) {
    if (numb % 2 === 0) { // перевірка чи число ділиться без остатка на 2
        return true; // повернення тру якщо так
    }
    return false; // повернення фолс в любому другому випадку
}

function checkIfAttrExist(obj, attr = 'first_name') {
    const allAttributes = Object.keys(obj); // получити всі атрибути об'єкта
    const result = allAttributes.find(function (key) { // пошук конкретного ключа в масиві всіх ключів
        return key === attr; // зрівняння ключа який находиться зараз в іттерації з іскомим ключем
    })
    return Boolean(result);
}